

#Arreglos  - Mapa de Puerto Rico




![main1.png](images/main1.png)
![main2.png](images/main2.png)



Los arreglos de datos (*arrays*) nos facilitan guardar y trabajar con grupos de datos del mismo tipo. Los datos se guardan en espacios de memoria consecutivos a los que se puede acceder utilizando el nombre del arreglo con índices o suscritos que indican la posición en que se encuentra el dato. Las estructuras de repetición nos proveen una manera simple de acceder a los datos de un arreglo. 


Un *objeto* es una entidad que se utiliza en muchos lenguajes de programación para integrar los datos y el código que opera en ellos, haciendo más fácil el modificar programas grandes. Una tarea bien común en programación usando C++ es el trabajar con arreglos de objetos. En la experiencia de laboratorio de hoy estarás trabajando con datos *georeferenciados* de pueblos en Puerto Rico, en donde tendrás atributos, como el nombre del pueblo, la latitud y longitud de su localización, que utilizarás para ilustrar propiedades en un mapa.




##Objetivos:


1. Crear dinámicamente y manipular un arreglo de objetos.


2. Codificar funciones para procesar arreglos de objetos.


3. Practicar el pasar arreglos de objetos como parámetros de una función.


4. Practicar la lectura secuencial de datos en un archivo.


5. Usar programación modular.


6. Usar estructuras de repetición y control.




##Pre-Lab:


Antes de llegar al laboratorio debes haber:


1. Repasado los conceptos relacionados a arreglos de objetos.


2. Repasado los conceptos relacionados con funciones que utilizan arreglos de objetos.


3. Repasado cómo leer datos de un archivo.


4. Estudiado los conceptos e instrucciones para la sesión de laboratorio.


5. Tomado el quiz Pre-Lab, disponible en Moodle.


---


---




##Datos georeferenciados


Trabajar con arreglos de objetos es una tarea bien común en la programación usando C++. Una vez has leído la información de los objetos de un archivo o provenientes de un usuario, debes depender de tus destrezas algorítmicas y conocimiento sobre C++ para invocar los métodos y funciones adecuadas para procesar los datos correctamente.


En esta experiencia de laboratorio estarás trabajando con datos *georeferenciados* sobre las ciudades en Puerto Rico. El que un dato sea *georeferenciado* quiere decir que el dato tiene una localización física asociada. Típicamente, esta **localización** son coordenadas de longitud y latitud. Por ejemplo, lo que sigue es parte de un archivo que contiene datos georeferenciados de pueblos en Puerto Rico:


---


```
Arroyo 17.9658 -66.0614
Bayamon 18.3833 -66.15
Caguas 18.2342 -66.0486
Dorado 18.4589 -66.2678
Fajardo 18.3258 -65.6525
```


**Figura 1.** Parte del contenido de una archivo de datos georeferenciados de pueblos en Puerto Rico; contiene nombre del pueblo, coordenada de latitud y coordenada de longitud.


---


###Distancia ortodrómica


Para calcular la distancia entre dos puntos en el plano Euclidiano, trazas el segmento de  línea recta que une a los puntos y calculas su largo utilizando la fórmula de distancia que estudiaste en tu curso de Pre-cálculo. Para calcular la distancia entre dos puntos en la superficie de una esfera no utilizas el segmento de línea que los une, utilizas la distancia más corta entre esos puntos medidos sobre la esfera. Esta distancia se llama *distancia ortodrómica*. Para calcular la distancia ortodrómica entre dos puntos en el globo terráqueo se usan las coordenadas de latitud y longitud.




###La clase `GISPOI`


La manera más común para los programadores en C++ encapsular datos asociados a un ente es utilizando **clases**. Por ejemplo, en el caso de datos georeferenciados, una manera práctica de encapsular la información para cada pueblo sería implementando una clase `GeoreferencedPointOfInterest` que contenga al menos datos (o atributos) para: el nombre del pueblo, su latitud y su longitud. La clase `GPOI` también necesitará implementar métodos para acceder, modificar y hacer operaciones en sus atributos.


En esta experiencia de laboratorio te proveemos una clase `GPOI`  con los siguientes métodos de interfaz:




* `GISPOI()`: constructor por defecto


* `GISPOI(QString s, double latitude, double longitude)`:  constructor que recibe nombre, longitud y latitud


* `double getLat()`, `double getLon()`:  "getters" para la latitud y longitud


* ` QString getName()`:  "getter" para el nombre


* `void setAll(string s, double a, double b)`: "setter" para todas las propiedades (a la vez) 


* `double odDistance(const GISPOI &B) const`: dado otro objeto `B` de la clase  `GPOI`, devuelve la distancia *ortodrómica* (*orthodromic*) (la distancia más corta) entre el `GPOI` que la invoca y `B`


---

---



## Leyendo datos de un archivo de texto en C++

En este laboratorio crearás un programa que lee datos de un archivo de texto.  Puedes saltar esta sección si te sientes cómodo con tus destrezas de manejo de archivos de texto en C++. De lo contrario, sigue leyendo...

C++ provee funciones para leer y escribir datos en archivos. En este laboratorio usarás uno de los métodos más básicos de lectura: lectura secuencial de archivos de texto. Los **archivos de texto** consisten de caracteres ASCII que representan datos en alguno de los tipos primitivos de C++. Por ejemplo, supón que el archivo `nameAge.txt` contiene algunos datos sobre nombres y edades.

```
Tomas 34
Marta 55
Remigio 88
Andrea 43
```

Para **leer** un archivo de texto como parte de un programa en C++, debemos conocer cómo están organizados los datos en el archivo y qué tipo de datos deseamos leer. El archivo ejemplo `nameAge.txt` contiene cuatro líneas y cada línea contiene un string y un entero. A continuación un programa para leer el archivo de principio a fin mientras se imprimen los datos que se van leyendo en cada línea. Lee los comentarios del programa para que entiendas sus partes:


```

#include <iostream>

// fstream es el header file que contiene clases, funciones y objetos 
// para trabjar con lectura y escritura de archivos.
#include <fstream>  

using namespace std;

int main(){

    // Usaremos las siguientes dos variables para asignarle los
    // valores que leemos en cada linea del archivo.
    string name;
    int age;
    
    // Definimos el objeto que representará al archivo 
    ifstream inFile;
    
    // Invocamos a la funcion open para que abra el file `nameAge.txt` 
    inFile.open("nameAge.txt");
    
    
    // Verificamos que el archivo ha sido debidamente abierto
    if (!inFile.is_open()) {
        cout << "Error openning file nameAge.txt\n";
        exit(1);
    }

    
    // Mientras haya datos por leer en el archivo, leer un string
    // y un entero. Observa como usamos simbolo `>>`, parecido
    // a como lo usamos con cin.
    
    while (inFile  >> name >> age) {
        cout << name << " : " << age << endl;
    }
    
    // Cerrar el archivo. 
    inFile.close();
    
    return 0;
}
```

El objeto `ifstream` permite que leamos el archivo de forma **secuencial**. Lleva cuenta de la próxima posición a leer dentro del archivo. Cada vez que leemos uno o más datos (usando `inFile >> ____`) el objeto adelanta su posición para que el próximo `inFile >> ___` pueda leer los datos siguientes.

Observa la linea `inFile  >> name >> age`. Esa instrucción realiza varias tareas:

* Lee un `string` y un `int` del archivo (si queda algo por leer) y se los asigna a las variables `name` y `age`.
* Si pudo leer ambos datos, la expresión `inFile  >> name >> age` evalúa `true`.
* De lo contrario, la expresión evalúa `false`, saliendo del bloque while.

A continuación algunos pedazos de código C++ para tareas comunes de lectura de archivos. Observa que en todas ellas:

1. Creamos un objecto de clase `ifstream`, llamamos a su función `open` y verificamos que el archivo abrió correctamente.
2. Creamos una o más variables para asignarles los valores que leeremos del archivo.
3. Implementamos un ciclo que se repite mientras hayan datos que leer del archivo.
4. Cerramos (función `close`) el archivo.



**Ejemplo 1**: Leer un archivo que solo contiene datos enteros y acumular sus valores.

```
    ifstream inFile;
    int n;
    int accum = 0;
    
    inFile.open("nums.txt");

    if (!inFile.is_open()) {
        cout << "Error openning file nums.txt\n";
        exit(1);
    }

    while (inFile  >> n) {
        accum = accum + n;
    }

    cout << "Total: "  << accum << endl;
    
    inFile.close();
```

**Ejemplo 2**: Contar el número de líneas en un archivo que contiene  un nombre por línea. Luego imprimir el contenido de la línea del medio.

```
    ifstream inFile;
    string name;
    int ctr = 0;
    
    inFile.open("names.txt");

    if (!inFile.is_open()) {
        cout << "Error openning file names.txt\n";
        exit(1);
    }

    while (inFile  >> name) {
        ctr++;
    }

    cout << "Total number of lines: " << ctr << endl;

    // Estos dos comandos retroceden el file al principio.
    inFile.clear();
    inFile.seekg(0);

    for (int i = 0; i <= ctr / 2; i++) {
        inFile >> name;
    }

    cout << "The name at the position " << ctr / 2 << ": " << name << endl;

    inFile.close();
```



---

---


!INCLUDE "../../eip-diagnostic/PRMap/es/diag-prmap-01.html"
<br>

!INCLUDE "../../eip-diagnostic/PRMap/es/diag-prmap-02.html"
<br>

!INCLUDE "../../eip-diagnostic/PRMap/es/diag-prmap-03.html"
<br>

---

---





##Sesión de laboratorio:


###Ejercicio 1 - Bajar y entender el código


####Instrucciones


1. Carga a `QtCreator` el proyecto `prMap`. Hay dos maneras de hacer esto:

    * Utilizando la máquina virtual: Haz doble “click” en el archivo `prMap.pro` que se encuentra  en el directorio `/home/eip/labs/arrays-prmap` de la máquina virtual.
    * Descargando la carpeta del proyecto de `Bitbucket`: Utiliza un terminal y escribe el commando `git clone http:/bitbucket.org/eip-uprrp/arrays-prmap` para descargar la carpeta `arrays-prmap` de `Bitbucket`. En esa carpeta, haz doble “click” en el archivo `prMap.pro`.

2. Compila y corre el programa. En su estado actual, el programa solo despliega un mapa de Puerto Rico. En este mapa podrás ver los resultados de tu programa. Quizás ves algunas advertencias ("warnings") debido a que hay algunas funciones que están incompletas. Estarás completando estas funciones durante la experiencia de laboratorio.

3. Abre el archivo `main.cpp`. En este archivo es que estarás escribiendo tu código. El archivo contiene las siguientes funciones:
 
    1. `void printArrayOfCities(GISPOI A[], int size)`: Dado un arreglo `A` de objetos de la clase `GISPOI` y su tamaño, imprime todos los pueblos en el arreglo. Puedes usar esta función como parte de tu proceso de depuración ("debugging").
    
    2. `int countLinesInFile(ifstream &file)`: Dada una referencia a un objeto que representa un archivo, esta función cuenta y devuelve el número de filas del archivo.

    3. `void readFileToArray(ifstream &file, GISPOI A[], int numOfCities)`: Dado el objeto `ifstream` de un archivo, un arreglo de pueblos, y el número de registros para leer del archivo, esta función lee los valores del archivo y llena el arreglo con objetos. **Esta es una función que tú implementarás**.

    4. `void maxDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)` : Dado un arreglo `A` de pueblos, determina los dos pueblos que quedan más lejos. Recuerda que la distancia que calcularás será la distancia *ortodrómica*. La función devuelve (por referencia) los índices de estas ciudades en el arreglo. **Esta es una función que tú implementarás**.

    5. `void minDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`: Dado un arreglo `A` de pueblos, determina los dos pueblos que quedan más cerca. Recuerda que la distancia que calcularás será la distancia *ortodrómica*. La función devuelve (por referencia) los índices de estas ciudades en el arreglo. **Esta es una función que tú implementarás**.

    6. `double cycleDistance(GISPOI A[], int size, int P[])`: Dado un arreglo `A` de pueblos, el tamaño de este arreglo, y un arreglo `P` con una permutación de los enteros en `[0,size-1]`, computa y devuelve la distancia de viajar el ciclo de pueblos `A[P[0]]` $$\rightarrow$$ `A[P[1]]` $$\rightarrow \cdots \rightarrow$$ `A[P[size-1]]`. Recuerda que la distancia que calcularás será la distancia *ortodrómica*.
  
        Por ejemplo, si los pueblos que se leen del archivo fueran Mayagüez, Ponce, Yauco y San Juan (en ese orden) y la permutación `P` es $$(3, 1, 0, 2)$$, la función debe computar la distancia del ciclo San Juan $$\rightarrow$$ Ponce $$\rightarrow$$ Mayagüez $$\rightarrow$$ Yauco $$\rightarrow$$ San Juan.**Esta es una función que tú implementarás**.


Hay otras dos funciones que debes conocer:

1. `void MainWindow::drawLine(const GISPOI &city01, const GISPOI &city02)`: Dada una referencia a dos objetos `GISPOI`, la función pinta una línea entre ellos.

2. `void drawPoints(GISPOI* gisLocations, unsigned int size);`: Dado un arreglo de objetos `GISPOI` y su tamaño, despliega sus localizaciones como puntos en el mapa.


### Ejercicio 2 - Leer los puntos georeferenciados a un arreglo 


Recuerda que solo estarás cambiando código en el archivo `main.cpp`. Tu primera tarea será añadir código para leer todo el contenido de un archivo a un arreglo de objetos `GISPOI`.



1. En la función `main()`, añade las instrucciones necesarias para abrir el archivo que contiene la información de los pueblos georeferenciados. El archivo que usarás primero está en el directorio `data` y se llama `pr10.txt`. Necesitas dar el `path` completo del archivo como parámetro del método `open()` de tu objeto `ifstream`. Como siempre, cuando uses archivos debes verificar si el nombre del archivo que pusiste se puede abrir para leer exitosamente.


2. Invoca la función `int countLinesInFile(ifstream &inFile)` para obtener el número de líneas en el archivo. Puedes imprimir el número de líneas obtenido de modo que verifiques que tu programa está funcionando correctamente.


3. Crea un arreglo **dinámicamente** tan grande como el número de líneas en tu programa.


4. Modifica la función `void readFileToArray(ifstream &file, GISPOI A[], int numOfCities)` de modo que lea todas las líneas en el archivo a los objetos en el arreglo.


5. En la función `main()`, invoca la función `readFileToArray`, pasando la referencia al archivo, el arreglo que creaste en el paso 3, y su tamaño.


6. Luego de invocar la función `readFileToArray` puedes invocar la función `void printArrayOfCities(GISPOI A[], int size)` para imprimir los nombres y georeferencias de los puntos leídos del archivo.


7. Invoca el método `drawPoints(GISPOI* gisLocations, unsigned int size)` en el objeto `w` de modo que se muestre un punto en el mapa para cada pueblo. La invocación debe hacerse de esta manera: `w.drawPoints(A, size)` (*asumiendo que `A` es el nombre de tu arreglo*). Debes obtener algo parecido a la siguiente figura:

    ![main1.png](images/main1.png)


   




### Ejercicio 3 - Funciones max y min 


Una vez que tengas la información de los pueblos georeferenciados en el arreglo de objetos, puedes comenzar a procesarlos de muchas formas interesantes. Comenzaremos con algunas operaciones básicas.


1. Lee la documentación e implementa la función `void maxDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`. Invoca la función desde `main()`.


2. Usa el método `void drawLine(const GISPOI &city01, const GISPOI &city02)` del objeto `w` para dibujar una línea que conecte los pueblos más lejanos. Nota que el segundo y tercer parámetro de este método son **referencias a los objetos que representan los pueblos** (no sus índices en el arreglo).


3. Lee la documentación e implementa la función `void minDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`. Invoca la función desde `main()`.


4. Usa el método `void drawLine(const GISPOI &city01, const GISPOI &city02)` del objeto `w` para dibujar una línea que conecte los pueblos más cercanos.




### Ejercicio 4 - Computa la distancia del ciclo


1. Lee la documentación e implementa la función `double cycleDistance(GISPOI A[], int size, int P[])`. Invoca la función desde `main()` como se indica en los comentarios dentro de la función `main()`:

      * Primero, con $$P =(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)$$,
      * Luego, con = $$P=(0, 3, 6, 9, 1, 4, 7, 2, 5, 8)$$. 




### Ejercicio 5 - ¡Más diversión!


Cambia tu código de modo que ahora abra el archivo `pr.txt`. Valida tus resultados y ¡maravíllate de tu gran logro!




---


---


##Entrega


Utiliza "Entrega" en Moodle para entregar el archivo `main.cpp`. Recuerda utilizar buenas prácticas de programación, incluye el nombre de los programadores y documenta tu programa.






---


---




## Referencias


[1] https://en.wikipedia.org/wiki/Great-circle_distance


---


---


---