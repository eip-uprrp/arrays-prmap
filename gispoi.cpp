#include "gispoi.h"


double GISPOI::odDistance(const GISPOI &B) const {
    return EARTH_RADIUS * acos(
        sin( deg2rad( getLat() ) ) * sin( deg2rad( B.getLat() ) ) +
        cos( deg2rad( getLat() ) ) * cos( deg2rad( B.getLat() ) ) *
        cos( deg2rad( getLon() ) - deg2rad(B.getLon()) ) );
}
