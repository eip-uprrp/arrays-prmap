#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "map.h"
namespace Ui {
class MainWindow;
}

/// \brief The mainwindow for the prMap application.


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
	// data members, I will declare as public for now
    Map *myMap;

	/// \fn MainWindow(QWidget *parent = 0)
    /// \~English
    /// \brief default constructor
    /// \param parent a pointer to this object's parent
    /// \~Spanish
    /// \brief constructor por defecto
    /// \param parent puntero al padre de este objeto
    ///
    explicit MainWindow(QWidget *parent = 0);

	/// \fn void drawPoints(GISPOI* gisLocations, unsigned int size)
    /// \~English
    /// \brief receives an array of points and its size, assigns it to
    ///        an array inside the myMap object.
    /// \param gisLocations an array of GISPOI objects in which we would
    ///                     like to draw points
    /// \param size size of the array
    /// \~Spanish
    /// \brief recibe un arreglo de puntos y su tamañano y lo asigna
    ///        a un arreglo dentro del objeto myMap.
    /// \param gisLocations un arreglo de objetos de clase GISPOI de los que 
    ///                     desearíamos dibujar puntos.
    /// \param size tamaño del array
    ///
    void drawPoints(GISPOI* gisLocations, unsigned int size);

	/// \fn void drawLine(const GISPOI &city01, const GISPOI &city02)
    /// \~English
    /// \brief receives reference to two objects of class GISPOIO and 
    ///        registers that a segment shall be painted between them in the map.
    /// \param poi01 one of the two GISPOI object ends of the segment  
    /// \param poi02 the other of the two GISPOI object ends of the segment
    /// \~Spanish
    /// \brief recibe referencia a dos objetos de clase GISPOI y registra que 
    ///        que se pintará una linea entre ellos dentro del mapa.
    /// \param poi01 uno de los dos puntos extremos del segmento.
    /// \param poi02 el otro de los dos puntos extremos del segmento.
    ///
    void drawLine(const GISPOI &poi01, const GISPOI &poi02);

	/// \fn ~MainWindow()
    /// \~English
    /// \brief destructor
    /// \~Spanish
    /// \brief destructor
    ///
    ~MainWindow();

private:
    Ui::MainWindow *ui;

protected:
};

#endif // MAINWINDOW_H
