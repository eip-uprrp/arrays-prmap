#ifndef COUNTRY_H
#define COUNTRY_H

#include <QPointF>
#include <QMap>
#include <QPolygonF>

/// \brief A class to display a map of a country, given the coordinates
/// that define its limits.

class Country {
private:
public:
    QMap<QString,QPolygonF*> Cities;
    double minX, minY, maxX, maxY;

    /// \fn Country()
    /// \~English
    /// \brief default constructor
    /// \~Spanish
    /// \brief default constructor
    ///
    Country() {}

    /// \fn bool readInfoFromJSON(QString fileName)
    /// \~English
    /// \brief Reads a json file that contains the polygons for the
    ///        various parts of the country (e.g., cities)
    /// \param fileName name of the json file
    /// \~Spanish
    /// \brief Lee un archivo en formato json que contiene los polígonos
    ///        the los componentes del mapa del pais (e.g. las ciuidades)
    /// \param fileName nombre del archivo en formato json
    ///
    bool readInfoFromJSON(const QString &fileName);

    /// \fn void limits();
    /// \~English
    /// \brief Determines the coordinates of the top left and bottom right
    ///        corners of the map.
    /// \~Spanish 
    /// \brief Determina las coordenadas de las esquinas superior izquierda
    ///        e inferior derecha del mapa.
    ///
    void limits();

};


#endif // COUNTRY_H
