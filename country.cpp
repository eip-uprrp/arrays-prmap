#include "country.h"
#include <QFile>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>



void Country::limits(){
    QMap<QString,QPolygonF*>::iterator it = Cities.begin();

    minX = maxX = Cities.begin().value()->at(0).x();
    minY = maxY = Cities.begin().value()->at(0).y();

    for (it = Cities.begin() + 1; it != Cities.end(); ++it) {

        QPolygonF *c = it.value();

        for (int i = 0; i < c->size(); i++) {
            if (c->at(i).x() != 0.0) {
            if (c->at(i).x() > maxX ) maxX = c->at(i).x();
            if (c->at(i).y() > maxY ) maxY = c->at(i).y();
            if (c->at(i).x() < minX ) minX = c->at(i).x();
            if (c->at(i).y() < minY ) minY = c->at(i).y();
            }
        }
    }
    qDebug() << minX << minY << maxX << maxY ;
}


unsigned long  *colorMap;
bool Country::readInfoFromJSON(const QString &fileName) {
    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open read file.");
        return false;
    }

    qDebug() << "JSON ok...";

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonArray featureLevelArray = loadDoc.object()["pueblos"].toObject()["features"].toArray();
    int ctr = 1;
    foreach(QJsonValue obj, featureLevelArray) {
        QPolygonF *poly = new QPolygonF;
        QJsonArray geometryArray = obj.toObject()["geometry"].toObject()["coordinates"].toArray();
        QString polyType = obj.toObject()["geometry"].toObject()["type"].toString();
        qDebug() << polyType;
        if (polyType == "Polygon") {
            for (int i = 0; i < geometryArray[0].toArray().size(); i++) {
                poly->push_back(QPointF(
                                geometryArray[0].toArray()[i].toArray()[0].toDouble(),
                                geometryArray[0].toArray()[i].toArray()[1].toDouble()));
            qDebug() << geometryArray[0].toArray()[i].toArray()[0].toDouble();
            }

        }
        else {
            // for every city
            for (int i = 0; i <geometryArray.size() ; i++) {
                // for every point in the city's polygon
                for (int j = 0; j < geometryArray[i].toArray()[0].toArray().size(); j++) {
                    poly->push_back(QPointF(
                                          geometryArray[i].toArray()[0].toArray()[j].toArray()[0].toDouble(),
                                          geometryArray[i].toArray()[0].toArray()[j].toArray()[1].toDouble()));

                }
                // I will push a 0,0 between each polygon of a multipolygon to distinguish
                poly->push_back(QPointF(0,0));
            }
        }

        QString cityName = obj.toObject()["properties"].toObject()["NAME"].toString();

        qDebug() << "Storing: " << cityName;
        Cities[cityName] = poly;
        ctr++;
    }
    return true;
}
