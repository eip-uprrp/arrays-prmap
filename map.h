#ifndef MAP_H
#define MAP_H

#include <QWidget>
#include <QMap>
#include <QRgb>
#include <country.h>
#include <gispoi.h>
#include <QVector>
#include <QPair>

/// This is the actual widget in charge of drawing the map
/// Este es el "widget" encargado de dibujar el mapa


class Map : public QWidget
{
    Q_OBJECT
public:
    /// \fn Map(QWidget *parent = 0);
    /// \~English
    /// \brief default constructor
    /// \~Spanish
    /// \brief constructor por defecto
    ///
    explicit Map(QWidget *parent = 0);

    /// \fn drawPoints(GISPOI *v, unsigned int size);
    /// \~English 
    /// \brief sets the array of GIS locations and sets its size
    /// \~Spanish
    /// \brief establece el arreglo de localizaciones de GIS y su tamaño
    ///
    void drawPoints(GISPOI *v, unsigned int size) 
                { gisLocations = v; numLocations = size; }
    
    /// \fn drawLine(const GISPOI &city01, const GISPOI &city02)
    /// \~English
    /// \brief adds a line from poi01 to poi02
    /// \~Spanish
    /// \brief añade una linea entre los centroides de city01 a city02
    ///
    void drawLine(const GISPOI &poi01, const GISPOI &poi02) {
        this->cityLines.push_back(
            QPair<const GISPOI *, const GISPOI *> (&poi01, &poi02));
    }
    
    /// \fn ~Map)()
    /// \~English
    /// \brief destructor
    /// \~Spanish
    /// \brief destructor
    ///
    ~Map();
signals:

private:
    Country            *myCountry;
    QMap<QString,QRgb> *cityColorMap;
    GISPOI             *gisLocations;
    unsigned int       numLocations;
    bool               drawRoutes;
    QVector < QPair<const GISPOI *,const GISPOI *> > cityLines;

protected:

    /// \fn paintEvent(QPaintEvent *event);
    /// \~English
    /// \brief function that is called every time the map widget 
    ///        generates a paint event. It draws the polygons and lines
    ///        of the map.
    /// \~Spanish
    /// \brief función que se invoca cada vez que el widget genera un
    ///        evento de pintar. Dibuja los polígonos y las lineas en el
    ///        mapa.
    ///
    void paintEvent(QPaintEvent *event);

public slots:

};

#endif // MAP_H
