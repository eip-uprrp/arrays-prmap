#define QT_NO_DEBUG_OUTPUT


#include "mainwindow.h"
#include <QApplication>
#include <cmath>
#include <cassert>
#include <fstream>

using namespace std;



// Function unitTests:
// Some unit tests for the distance function

// void unitTests(){
//     GISPOI ny(    "NewYork",   40.7142,  -74.0064 );
//     GISPOI london("London",    51.50,    -0.1167 );
//     GISPOI la(    "LA",        34.0522,  -118.2428);

//     assert(abs( ny.odDistance(london) - 5576.74) < 10);
//     assert(abs( ny.odDistance(la) -     3940.14) < 10);
//     assert(abs( ny.odDistance(ny) -           0) < 100);

//     cout << "Passed all unit tests...\n";
// }

// Function printArrayOfCities(A, size):
// Given A, an array of GISPOI objects and its size, prints
// all the cities in the array.

void printArrayOfCities(GISPOI A[], int size) {
    for (int i = 0; i < size; i++)
        A[i].print();
}

// Function countLinesInFile(f)
// Given a reference to the object that represents a file, this function
// counts and returns the number of lines in the file.

int countLinesInFile(ifstream &file) {
    string dummy;
    int ctr = 0;

    while( getline(file,dummy)) ctr++;

    // 'reset' the file, so that it can be reread from the start
    file.clear();
    file.seekg(0, ios::beg) ;
    return ctr;
}

// Function readFileToArray(file, A, numCities):
// Given the ifstream object of a file, an (empty) array of cities
// and the number of records to read from the file, this function
// reads the values from the file and populates the array with
// objects.

void readFileToArray(ifstream &file, GISPOI A[], int numOfCities) {
    // your code here
    cout << "Reading the file and storing in array of cities.."
            "(your job)" << endl;

}

// Function maxDistances(A, size, idxCityA, idxCityB):
// Given the array of cities, determines the farthest two cities.
// Returns (by reference) the indices of the two cities in the array.

void maxDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB){
    // your code here
}


// Function minDistances(A, size, idxCityA, idxCityB):
// Given the array of cities, determines the closest two cities.
// Returns (by reference) the indices of the two cities in the array.
void minDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB){
    // your code here
    cout << "Computing the two closest cities..." <<
            "(your job)" << endl;
}


// Function cycleDistance (A, size, P):
// Given the array of cities, the size of the array and a array (P) with a
// permutation of the integers [0, size-1] computes and returns the distance
// to travel the cycle of cities A[P[0]] -- A[P[1]] -- .... -- A[P[size-1]]

double cycleDistance(GISPOI A[], int size, int P[]){
    // your code here
    return 0.0;
}


// The main program can be summarized with this pseudocode:
// 1. Reads the contents of the file to an array of structures.
// 2. Computes the cities with max distance.
// 3. Computes the cities with min distance.
// 4. Computes the total distance of traveling the cities in a certain order.


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    cout << "Welcome to PR map!!!!" << endl;
    // open the file and check its validity
    // you need to use inFile.open(fileName.c_str()) instead of inFile.open(fileName)
    // since the open method expects a c-type string instead of a c++ string object 

    // count the number of lines in the file

    // create an array of GISPOI objects with exactly as many elements as there
    // are lines in the file.

    // read the data from the file into the array of structures


    // paint the points onto the map window

    // Here: Put the code to invoke the max distance function and show a line
    // in the map between those two cities.


    // Here: Put the code to invoke the min distance function and show a line
    // in the map between those two cities.


    // Here: Put the code to compute the cycle distance using these permutation
    // P = {0, 2, 4, 6, 8, 1, 3, 5, 7, 9}
    // P = {0, 3, 6, 9, 1, 4, 7, 2, 5, 8}

    return a.exec();
}
