#ifndef GISPOI_H
#define GISPOI_H
#include <QString>
#include <iostream>
#include <QDebug>
#include <cmath>
#include <QPointF>
using namespace std;

const double EARTH_RADIUS =  6372.8;
const double TWOPI = 2 * acos(-1);

///
/// \brief deg2rad
/// \~English
/// \param deg angle in degrees
/// \return angle in radians
/// \~Spanish
/// \param deg ángulo en grados
/// \return angle ángulo en radianes
///
inline double deg2rad(double deg) {
    return deg/360.0 * TWOPI;
}


///
/// \brief This class is for representing GIS points of interests.
///

class GISPOI {
private:
    QString name;
    double lat, lon;

public:

    /// \fn GISPOI()
    /// \~English
    /// \brief default constructor
    /// \~Spanish
    /// \brief constructor por defecto
    ///
    GISPOI(){};

    /// \fn GISPOI(const QString &s, double latitude, double longitude)
    /// \~English
    /// \brief constructor that sets all the data members.
    /// \~Spanish
    /// \brief constructor que asigna valores a los miembros de data.
    ///
    GISPOI(const QString &s, double latitude, double longitude)
        {name = s; lat = latitude; lon = longitude;}


    /// \fn double getLat()
    /// \~English
    /// \brief getter for the latitude
    /// \return the latitude value
    /// \~Spanish
    /// \brief "getter" para el valor de latitud
    /// \return el valor de la latitud
    ///
    double getLat() const {return lat;}

    /// \fn double getLon()
    /// \~English
    /// \brief getter for the longitude
    /// \return the longitude value
    /// \~Spanish
    /// \brief "getter" para el valor de longitud
    /// \return el valor de la longitud
    ///
    double getLon() const {return lon;}

    /// \fn QString getName()
    /// \~English
    /// \brief getter for the name
    /// \return a copy of the name
    /// \~Spanish
    /// \brief "getter" para el nombre
    /// \return una copia del nombre
    ///
    QString getName() const {return name;}
    
    /// \fn setAll(const QString &s, double latitude, double longitude)
    /// \~English
    /// \brief setter for all data members
    /// \~Spanish
    /// \brief "setter" para todos los miembros de datos
    ///
    void setAll(QString s, double latitude, double longitude)
        {name = s; lat = latitude; lon = longitude;}

    /// \fn setAll(const string &s, double latitude, double longitude)
    /// \~English
    /// \brief setter for all data members
    /// \~Spanish
    /// \brief "setter" para todos los miembros de datos
    ///
    void setAll(string s, double latitude, double longitude) 
        {name = QString::fromStdString(s); lat = latitude; lon = longitude;}
    

    /// \fn print()
    /// \~English
    /// \brief a utility function to print the data members
    /// \~Spanish
    /// \brief imprime los valores de los miembros de datos
    ///
    void print() {
        qDebug() << name << " " << lat  << " "
             << lon << endl;
    }

    /// \fn print()
    /// \~English
    /// \brief Given two objects A and B of class GISPOI, uses their
    ///        longitudes and latitudes to compute and return their orthodromic
    ///        distance in kilometers.
    /// \returns orthodromic distance in kilometers
    /// \~Spanish
    /// \brief Dados dos objetos A y B de clase GISPOI, usa sus
    ///        longitudes and latitudes para determinar y devolver distancia
    ///        ortodrómica en kilómetros.
    /// \returns distancia ortodrómica en kilómetros
    ///
    double odDistance(const GISPOI &B) const;

};





#endif // GISPOI_H
