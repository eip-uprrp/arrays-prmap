#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <country.h>
#include <gispoi.h>

using namespace std;


Country PR;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    srand(time(NULL));
    myMap = new Map(this);
    myMap->show();

    ui->setupUi(this);

    // just hidding the toolbars to have a bigger drawing area.
    // solo se esta escondiendo las barras de herramientas para tener una mayor area para dibujar.
    QList<QToolBar *> toolbars = this->findChildren<QToolBar *>();
    foreach(QToolBar *t, toolbars) t->hide();
    QList<QStatusBar *> statusbars = this->findChildren<QStatusBar *>();
    foreach(QStatusBar *t, statusbars) t->hide();

    resize(myMap->width(), myMap->height());
}

void MainWindow::drawPoints(GISPOI* gisLocations, unsigned int size) {
    myMap->drawPoints(gisLocations, size);
}

void MainWindow::drawLine(const GISPOI &poi01, const GISPOI &poi02) {
    myMap->drawLine(poi01, poi02);
}

MainWindow::~MainWindow() {
    delete ui;
    if (myMap != NULL) delete myMap;
}

