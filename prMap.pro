#-------------------------------------------------
#
# Project created by QtCreator 2014-04-21T21:57:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = prMap
TEMPLATE = app

# CONFIG += c++11

SOURCES += main.cpp\
    gispoi.cpp\
        mainwindow.cpp \
    country.cpp \
    map.cpp


HEADERS  += mainwindow.h \
    country.h \
    map.h \
    gispoi.h

FORMS    += mainwindow.ui

RESOURCES += \
    prmap.qrc

DEFINES += QT_NO_DEBUG_OUTPUT
